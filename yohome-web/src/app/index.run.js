(function() {
  'use strict';

  angular
    .module('yohome')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
