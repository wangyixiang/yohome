/**
 * Created by wangyixiang from Nanchang now in Shanghai China.
 */

(function () {
  "use strict";
  angular.module("yohome")
    .service("getPhotos", function () {
      var photoLocationBase = "";

      var data = [
        "photos/thumb/(2003-12-31)fuyuanxingchun_02.thumb.jpg",
        "photos/thumb/100_1084.thumb.jpg",
        "photos/thumb/198708huangshangbeihai.thumb.jpg",
        "photos/thumb/DSCF0582.thumb.jpg",
        "photos/thumb/DSCF0851.thumb.jpg",
        "photos/thumb/DSCF1199.thumb.jpg",
        "photos/thumb/DSCF4019.thumb.jpg",
        "photos/thumb/DSCN0117.thumb.jpg",
        "photos/thumb/DSCN0639.thumb.jpg",
        "photos/thumb/DSCN1107.thumb.jpg",
        "photos/thumb/DSCN1407.thumb.jpg",
        "photos/thumb/IMG_0215.thumb.jpg",
        "photos/thumb/IMG_0267.thumb.jpg",
        "photos/thumb/WP_20140914_005.thumb.jpg",
        "photos/thumb/WP_20140920_028.thumb.jpg",
        "photos/thumb/WP_20140927_002.thumb.jpg"
      ];

      function getPhotos(){

      }

      function getThumbnails(){
        var thumbnails = [];
        angular.forEach(data, function (photo) {
          thumbnails.push(photoLocationBase + photo);
        });
        return thumbnails;
      }

      this.getThumbnails = getThumbnails;
    });
})();
