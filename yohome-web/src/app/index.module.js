(function() {
  'use strict';

  angular
    .module('yohome', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'ui.bootstrap']);

})();
