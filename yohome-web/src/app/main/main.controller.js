(function() {
  'use strict';

  angular
    .module('yohome')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, getPhotos) {
    $scope.thumbnailUrls = getPhotos.getThumbnails();
  }
})();
